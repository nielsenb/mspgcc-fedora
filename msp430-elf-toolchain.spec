%global target msp430-elf
%global gcc_version_base 9.2.0
%global gcc_version %{gcc_version_base}.50
%global ti_version_number 9_2_0_0
%global archive_name msp430-gcc-%{gcc_version}-source-full

%global configure_args_common --prefix=%{_prefix}/%{target} --target=%{target} --enable-languages=c,c++ --disable-nls --disable-rpath
%global configure_args_binutils %(echo --disable-{sim,gdb,werror})
%global configure_args_gcc --with-local-prefix=%{_prefix} --without-headers --enable-target-optspace --enable-newlib-nano-formatted-io
%global configure_args_gdb %(echo --disable-{binutils,gas,ld,gprof,etc} --without-{mpfr,lzma} --with-python=no)

%global base_path %(echo $PATH)

Name:		msp430-elf-toolchain
Version:	9.2.0.0
Release:	2%{?dist}
Summary:	Mitto Systems Limited / TI developed version of the GNU toolchain for the MSP430
License:	GPLv2
URL:		https://www.ti.com/tool/MSP430-GCC-OPENSOURCE
Source0:	http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/%{ti_version_number}/export/%{archive_name}.tar.bz2
Source1:	README.fedora

BuildRequires:	bison
BuildRequires:	pkgconfig(expat)
BuildRequires:	expat-static
BuildRequires:	expect
BuildRequires:	flex
BuildRequires:	gcc
BuildRequires:	gcc-c++
BuildRequires:	gmp-devel
BuildRequires:	libmpc-devel
%if 0%{?fedora} >= 32
BuildRequires:	pkgconfig(mpfr)
%else
BuildRequires:	mpfr-devel
%endif
BuildRequires:	pkgconfig(ncurses)
BuildRequires:	sed
BuildRequires:	texinfo
BuildRequires:	pkgconfig(zlib)

%description
TI has partnered with Mitto Systems Limited to bring you a new and fully
supported open source compiler. This free GCC compiler has no code size
limit. In addition, this compiler can be used standalone or within Code
Composer Studio.

%package -n msp430-elf-gcc
Summary: GCC for the MSP430
Provides: bundled(gnulib)

%description -n msp430-elf-gcc
This is a cross compiling version of GNU GCC, which can be used to
compile for the %{target} platform, instead of for the native %{_arch}
platform.

%package -n msp430-elf-gcc-c++
Summary: GCC C++ for the MSP430

%description -n msp430-elf-gcc-c++
This is a cross compiling version of g++, which can be used to
compile for the %{target} platform, instead of for the native %{_arch}
platform.

%package -n msp430-elf-gdb
Summary: GDB for the MSP430

%description -n msp430-elf-gdb
This is a version of GDB, the GNU Project debugger, for (remote)
debugging %{target} binaries. GDB allows you to see and modify what is
going on inside another program while it is executing.

%package -n msp430-elf-binutils
Summary: GCC for the MSP430

%description -n msp430-elf-binutils
This is a Cross Compiling version of GNU binutils, which can be used to
assemble and link binaries for the %{target} platform, instead of for the
native %{_arch} platform.

%prep
%setup -q -c -n %{name}

#Copy the README
cp -a %{SOURCE1} .

# Symlink libgloss / newlib
ln -sr %{archive_name}/newlib/libgloss %{archive_name}/gcc
ln -sr %{archive_name}/newlib/newlib %{archive_name}/gcc

# Workaround for issues stripping foreign binaries, taken from the avr-gcc spec
# https://bugzilla.redhat.com/show_bug.cgi?id=1175942
# Extract %%__os_install_post into os_install_post~
cat << \EOF > os_install_post~
%__os_install_post
EOF

# Generate customized brp-*scripts
cat os_install_post~ | while read a x y; do
case $a in
# Prevent brp-strip* from trying to handle foreign binaries
*/brp-strip*)
  b=$(basename $a)
  sed -e 's,find "*$RPM_BUILD_ROOT"*,find "$RPM_BUILD_ROOT" -not -path "%{buildroot}%{_prefix}/%{target}/lib/gcc/%{target}/%{gcc_version_base}/*" -not -path "%{buildroot}%{_prefix}/%{target}/%{target}/lib/*",' $a > $b
  chmod a+x $b
  ;;
esac
done

sed -e 's,^[ ]*/usr/lib/rpm.*/brp-strip,./brp-strip,' \
< os_install_post~ > os_install_post

%build
mkdir -p %{archive_name}/build
cd %{archive_name}/build

mkdir {binutils,gcc,gdb}

cd binutils
../../binutils/configure %configure_args_common %configure_args_binutils
%make_build
cd -

# Symlink binutils so we can build GCC
mkdir bin
ln -sr binutils/binutils/addr2line bin/msp430-elf-addr2line
ln -sr binutils/binutils/ar bin/msp430-elf-ar
ln -sr binutils/gas/as-new bin/msp430-elf-as
ln -sr binutils/binutils/cxxfilt bin/msp430-elf-cxxfilt
ln -sr binutils/binutils/elfedit bin/msp430-elf-elfedit
ln -sr binutils/gprof/gprof bin/msp430-elf-gprof
ln -sr binutils/ld/ld-new bin/msp430-elf-ld
ln -sr binutils/binutils/libtool bin/msp430-elf-libtool
ln -sr binutils/binutils/nm-new bin/msp430-elf-nm
ln -sr binutils/binutils/objcopy bin/msp430-elf-objcopy
ln -sr binutils/binutils/objdump bin/msp430-elf-objdump
ln -sr binutils/binutils/ranlib bin/msp430-elf-ranlib
ln -sr binutils/binutils/readelf bin/msp430-elf-readelf
ln -sr binutils/binutils/size bin/msp430-elf-size
ln -sr binutils/binutils/strings bin/msp430-elf-strings
ln -sr binutils/binutils/strip-new bin/msp430-elf-strip

cd gcc
PATH=$PWD/../bin:$PATH
../../gcc/configure %configure_args_common %configure_args_gcc
%make_build
# Reset the path
PATH=%{base_path}
cd -

cd gdb
../../gdb/configure %configure_args_common %configure_args_gdb
%make_build
cd -

%install
cd %{archive_name}/build

cd binutils
%make_install
cd -

cd gcc
PATH=$PWD/../bin:$PATH
%make_install
# Reset the path
PATH=%{base_path}
cd -

cd gdb
%make_install
cd -

# Non-prefixed libcc
rm %{buildroot}%{_prefix}/%{target}/%{_lib}/libcc1.*

# Redundant binaries
rm -r %{buildroot}%{_prefix}/%{target}/%{target}/bin

# No libtool archives
rm %{buildroot}%{_prefix}/%{target}/lib/gcc/%{target}/%{gcc_version_base}/plugin/*.la
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/*.la
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/exceptions/*.la
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/430/*.la
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/430/exceptions/*.la
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/large/*.la
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/large/exceptions/*.la
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/large/full-memory-range/*.la
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/large/full-memory-range/exceptions/*.la

# Install tools aren't useful for embedded targets
rm -r %{buildroot}%{_prefix}/%{target}/lib/gcc/%{target}/%{gcc_version_base}/install-tools
rm -r %{buildroot}%{_prefix}/%{target}/libexec/gcc/%{target}/%{gcc_version_base}/install-tools

# Clean up unwanted syscalls
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/aarch64-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/arm-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/amd64-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/freebsd.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/gdb-syscalls.dtd
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/i386-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/mips-n32-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/mips-n64-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/mips-o32-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/ppc-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/ppc64-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/s390-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/s390x-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/sparc-linux.xml
rm %{buildroot}%{_prefix}/%{target}/share/gdb/syscalls/sparc64-linux.xml

# Unnecessary header
rm %{buildroot}%{_prefix}/%{target}/include/gdb/jit-reader.h

# Unwanted python functions
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/*.py
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/exceptions/*.py
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/430/*.py
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/430/exceptions/*.py
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/large/*.py
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/large/exceptions/*.py
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/large/full-memory-range/*.py
rm %{buildroot}%{_prefix}/%{target}/%{target}/lib/large/full-memory-range/exceptions/*.py
rm -r %{buildroot}%{_prefix}/%{target}/share/gcc-%{gcc_version_base}/python
rm -r %{buildroot}%{_prefix}/%{target}/share/gdb/system-gdbinit

# Duplicate documentation
rm %{buildroot}%{_prefix}/%{target}/share/man/man1/*
rm %{buildroot}%{_prefix}/%{target}/share/man/man5/*
rm %{buildroot}%{_prefix}/%{target}/share/man/man7/*
rm %{buildroot}%{_prefix}/%{target}/share/info/*

#Symlink executables
mkdir -p %{buildroot}%{_bindir}
ln -sr %{buildroot}%{_prefix}/%{target}/bin/* %{buildroot}%{_bindir}

cd %{_builddir}/%{name}
%define __os_install_post . ./os_install_post

%files -n msp430-elf-gcc
%doc README.fedora
%license %{archive_name}/gcc/COPYING
%{_bindir}/%{target}-as
%{_bindir}/%{target}-gcc-%{gcc_version_base}
%{_bindir}/%{target}-gcc
%{_bindir}/%{target}-gcov
%{_bindir}/%{target}-gcov-dump
%{_prefix}/%{target}/bin/%{target}-as
%{_prefix}/%{target}/bin/%{target}-gcc-%{gcc_version_base}
%{_prefix}/%{target}/bin/%{target}-gcc
%{_prefix}/%{target}/bin/%{target}-gcov
%{_prefix}/%{target}/bin/%{target}-gcov-dump
%{_prefix}/%{target}/lib/libmsp430-elf-sim.a
%{_prefix}/%{target}/lib/gcc/%{target}/%{gcc_version_base}/*
%{_prefix}/%{target}/libexec/gcc/%{target}/%{gcc_version_base}/*
%{_prefix}/%{target}/%{target}/lib/*
%{_prefix}/%{target}/%{target}/include/*
%exclude %{_prefix}/%{target}/%{target}/include/c++
%exclude %{_prefix}/%{target}/%{target}/lib/libstdc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/libsupc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/430/libstdc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/430/libsupc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/large/libstdc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/large/libsupc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/large/full-memory-range/libstdc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/large/full-memory-range/libsupc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/exceptions/libstdc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/exceptions/libsupc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/430/exceptions/libstdc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/430/exceptions/libsupc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/large/exceptions/libstdc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/large/exceptions/libsupc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/large/full-memory-range/exceptions/libstdc++.a
%exclude %{_prefix}/%{target}/%{target}/lib/large/full-memory-range/exceptions/libsupc++.a

%files -n msp430-elf-gcc-c++
%license %{archive_name}/gcc/COPYING
%{_bindir}/%{target}-cpp
%{_bindir}/%{target}-c++
%{_bindir}/%{target}-g++
%{_prefix}/%{target}/bin/%{target}-cpp
%{_prefix}/%{target}/bin/%{target}-c++
%{_prefix}/%{target}/bin/%{target}-g++
%{_prefix}/%{target}/%{target}/include/c++
%{_prefix}/%{target}/%{target}/lib/libstdc++.a
%{_prefix}/%{target}/%{target}/lib/libsupc++.a
%{_prefix}/%{target}/%{target}/lib/430/libstdc++.a
%{_prefix}/%{target}/%{target}/lib/430/libsupc++.a
%{_prefix}/%{target}/%{target}/lib/large/libstdc++.a
%{_prefix}/%{target}/%{target}/lib/large/libsupc++.a
%{_prefix}/%{target}/%{target}/lib/large/full-memory-range/libstdc++.a
%{_prefix}/%{target}/%{target}/lib/large/full-memory-range/libsupc++.a
%{_prefix}/%{target}/%{target}/lib/exceptions/libstdc++.a
%{_prefix}/%{target}/%{target}/lib/exceptions/libsupc++.a
%{_prefix}/%{target}/%{target}/lib/430/exceptions/libstdc++.a
%{_prefix}/%{target}/%{target}/lib/430/exceptions/libsupc++.a
%{_prefix}/%{target}/%{target}/lib/large/exceptions/libstdc++.a
%{_prefix}/%{target}/%{target}/lib/large/exceptions/libsupc++.a
%{_prefix}/%{target}/%{target}/lib/large/full-memory-range/exceptions/libstdc++.a
%{_prefix}/%{target}/%{target}/lib/large/full-memory-range/exceptions/libsupc++.a

%files -n msp430-elf-gdb
%license %{archive_name}/gdb/COPYING
%{_bindir}/%{target}-gdb
%{_bindir}/%{target}-gdb-add-index
%{_prefix}/%{target}/bin/%{target}-gdb
%{_prefix}/%{target}/bin/%{target}-gdb-add-index

%files -n msp430-elf-binutils
%license %{archive_name}/binutils/COPYING
%{_bindir}/%{target}-addr2line
%{_bindir}/%{target}-ar
%{_bindir}/%{target}-c++filt
%{_bindir}/%{target}-elfedit
%{_bindir}/%{target}-gcov-tool
%{_bindir}/%{target}-gprof
%{_bindir}/%{target}-ld
%{_bindir}/%{target}-ld.bfd
%{_bindir}/%{target}-nm
%{_bindir}/%{target}-objcopy
%{_bindir}/%{target}-objdump
%{_bindir}/%{target}-ranlib
%{_bindir}/%{target}-readelf
%{_bindir}/%{target}-run
%{_bindir}/%{target}-size
%{_bindir}/%{target}-strings
%{_bindir}/%{target}-strip
%{_bindir}/%{target}-gcc-ar
%{_bindir}/%{target}-gcc-nm
%{_bindir}/%{target}-gcc-ranlib
%{_prefix}/%{target}/bin/%{target}-addr2line
%{_prefix}/%{target}/bin/%{target}-ar
%{_prefix}/%{target}/bin/%{target}-c++filt
%{_prefix}/%{target}/bin/%{target}-elfedit
%{_prefix}/%{target}/bin/%{target}-gcov-tool
%{_prefix}/%{target}/bin/%{target}-gprof
%{_prefix}/%{target}/bin/%{target}-ld
%{_prefix}/%{target}/bin/%{target}-ld.bfd
%{_prefix}/%{target}/bin/%{target}-nm
%{_prefix}/%{target}/bin/%{target}-objcopy
%{_prefix}/%{target}/bin/%{target}-objdump
%{_prefix}/%{target}/bin/%{target}-ranlib
%{_prefix}/%{target}/bin/%{target}-readelf
%{_prefix}/%{target}/bin/%{target}-run
%{_prefix}/%{target}/bin/%{target}-size
%{_prefix}/%{target}/bin/%{target}-strings
%{_prefix}/%{target}/bin/%{target}-strip
%{_prefix}/%{target}/bin/%{target}-gcc-ar
%{_prefix}/%{target}/bin/%{target}-gcc-nm
%{_prefix}/%{target}/bin/%{target}-gcc-ranlib

%changelog
* Fri Jul 10 2020 Brandon Nielsen <nielsenb@jetfuse.net> - 9.2.0.0-2
- Correct URLs
- Correct TI version number
- Remove deprecated Group tag
- Add --without-headers to GCC compile options to not rely on system headers
- Set prefix to include msp430-elf so toolchain installs to /usr/msp430-elf
- Modify strip fix to match avr-gcc fixing F33+ builds

* Fri Jul 10 2020 Brandon Nielsen <nielsenb@jetfuse.net> - 9.2.0.0-1
- Update to 9.2.0.0
- Use pkgconfig where possible
- Add dist
- Simplify strip disabling using bashism
- Modify strip disabling to use last arg to support brp-strp-lto for F33+
- BuildRequire bash to enable the above
- Drop obsoletes as msp430-gcc was retired

* Sun Sep 29 2019 Brandon Nielsen <nielsenb@jetfuse.net> - 8.2.0.52-1
- Update to 8.2.0.52

* Mon Jun 25 2018 Brandon Nielsen <nielsenb@jetfuse.net> - 6.0.1.0-1
- Update to 6.0.1.0, the first Mitto Systems Limited release

* Sun Oct 29 2017 Brandon Nielsen <nielsenb@jetfuse.net> - 5.1.0.0-1
- Update to 5.1.0.0
- Use %{optflags} without -Werror=format-security

* Tue May 2 2017 Brandon Nielsen <nielsenb@jetfuse.net> - 5.0.0.0-1
- Switch to TI / SOMNIUM Technology upstream
- Add obsoletes msp430-gcc to msp430-elf-gcc
- Don't package duplicate documentation

* Thu Dec 1 2016 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.0.0-3
- Fix remaining .gz manpage compression assumptions

* Wed Nov 30 2016 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.0.0-2
- Don't strip libsupc++.a
- Don't assume .gz manpage compression

* Wed Jun 29 2016 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.0.0-1
- Rename mspgcc package to msp430-elf-toolchain
- Remove redundant clean section
- Remove redundant rm -rf buildroot at beginning of install
- Remove unecessary source folders in prep

* Fri Jun 24 2016 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.0.0-3
- Explicitly require gcc / gcc-c++ as per Fedora packaging guidelines

* Fri Jun 10 2016 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.0.0-2
- Force -std=gnu++98 for building on Fedora 24
- Add gnu++98_filter.patch to ensure libstdc++-v3/src/c++11 CXXFLAGS
  are used
- Fix license

* Mon Oct 26 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.5.0.0-1
- Update to 3.5.0.0

* Fri Aug 14 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.4.5.1-1
- Update to 3.4.5.1

* Tue Jun 09 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.3.4-2
- Add patch to allow use of provided install macro

* Fri May 29 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.3.4-1
- Update to 3.3.4
- Force gnu89 for compiling on Fedora 22 with GCC 5

* Sun Feb 15 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.2.3-1
- Update to 3.2.3

* Mon Feb 09 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.2.2-2
- Workaround for strip issue

* Tue Jan 20 2015 Brandon Nielsen <nielsenb@jetfuse.net> - 3.2.2-1
- Initial release
