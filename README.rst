=======================
 msp430-gcc-elf-fedora
=======================

Specfile for packaging the TI / Mitto Systems Limited GCC compiler for the MSP430.

See the official `TI`_ page for more details.

msp430-elf-gcc, msp430-elf-gcc-c++, msp430-elf-gdb, and msp430-elf-binutils are available at the `msp430-elf-gcc copr repository`_.

.. _TI: http://www.ti.com/tool/msp430-gcc-opensource
.. _msp430-elf-gcc copr repository: https://copr.fedorainfracloud.org/coprs/nielsenb/msp430-development-tools/
